# pumox

# technology

- .net core sdk 3.1
- EF Core
- Postgresql

# Basic Auth:
- username:user,
- password:user

# Database
MainConnection in appsettings.json

Migracje powinny się stworzyć automatycznie ale jeśli z jakiś przyczyn tego nie zrobi apka to:

- dotnet ef migrations add Mig --project Pumox.Data --startup-project Pumox

w przypadku updade

- dotnet ef database update --project Pumox.Data

Przykładowe dane są już dodane w DataContext Configuration

# App
aplikacja jest uruchomiana na porcie :7000
 
- dotnet build

- dotnet run --project Pumox
