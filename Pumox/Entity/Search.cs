using System;
using System.Collections.Generic;
using Pumox.Data.Models;

namespace Pumox
{
    public class Search
    {
        public string Keyword { get; set; }

        public DateTime? EmployeeDateOfBirthFrom { get; set; }
        public DateTime? EmployeeDateOfBirthTo { get; set; }

        public ICollection<EJobTitle> EmployeeJobTitle { get; set; }
    }
}