﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Pumox;
using Pumox.Data;
using Pumox.Data.Models;

namespace pumox.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class CompaniesController : ControllerBase
    {
        private readonly DataContext _context;

        public CompaniesController(DataContext context)
        {
            _context = context;
        }

        // GET: api/Companies
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Company>>> GetCompanies()
        {
            return await _context.Companies.Include(company => company.Employees).ToListAsync();
        }

        // GET: api/Companies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Company>> GetCompany(int id)
        {
            var company = await _context.Companies.Include(i => i.Employees).FirstOrDefaultAsync(i => i.Id == id);

            if (company == null)
            {
                return NotFound();
            }

            return company;
        }

        // PUT: api/Companies/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCompany(int id, Company company)
        {
            if (id != company.Id)
            {
                return BadRequest();
            }

            _context.Entry(company).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CompanyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Companies
        [HttpPost]
        public async Task<ActionResult<Company>> PostCompany(Company company)
        {
            _context.Companies.Add(company);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCompany", new { id = company.Id }, "Id: " + company.Id);
        }

        [HttpPost("search")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<Company>>> GetCompanies(Search search)
        {

            var companies = from m in _context.Companies
                            select m;

            var key = search.Keyword;
            var dateFrom = search.EmployeeDateOfBirthFrom;
            var dateTo = search.EmployeeDateOfBirthTo;

            if (!String.IsNullOrEmpty(search.Keyword))
            {
                companies = companies.Where(s => s.Name.Contains(key) ||
                 s.Employees.Any(x => x.FirstName.Contains(key)) ||
                 s.Employees.Any(x => x.LastName.Contains(key)));
            }

            if (dateFrom.HasValue && !dateTo.HasValue)
            {
                companies = companies.Where(s => s.Employees.Any(x => x.DateOfBirth >= dateFrom));
            }

            if (!dateFrom.HasValue && dateTo.HasValue)
            {
                companies = companies.Where(s => s.Employees.Any(x => x.DateOfBirth <= dateTo));
            }

            if (dateFrom.HasValue && dateTo.HasValue)
            {
                companies = companies.Where(s => s.Employees.Any(x => x.DateOfBirth >= dateFrom && x.DateOfBirth <= dateTo));
            }

            if (search.EmployeeJobTitle != null)
            {
                companies = companies.Where(s => s.Employees.Any(x => search.EmployeeJobTitle.Contains(x.JobTitle)));
            }

            return await companies.Include(company => company.Employees).ToListAsync();
        }

        // DELETE: api/Companies/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Company>> DeleteCompany(int id)
        {
            var company = await _context.Companies.FindAsync(id);
            if (company == null)
            {
                return NotFound();
            }

            _context.Companies.Remove(company);
            await _context.SaveChangesAsync();

            return company;
        }

        private bool CompanyExists(int id)
        {
            return _context.Companies.Any(e => e.Id == id);
        }
    }
}
