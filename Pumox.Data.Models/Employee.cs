using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Pumox.Data.Models
{
    public class Employee : Entity
    {
        [Required]
		[StringLength(500)]
        public string FirstName { get; set; }

        [Required]
		[StringLength(500)]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yy}", ApplyFormatInEditMode = true)]
        public DateTime DateOfBirth {get;set;}

        [Required]
        public EJobTitle JobTitle { get; set; }        

        [JsonIgnore]
        public long? CompanyId { get; set; }

        public Company Company { get; set; }


	}
}