using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Pumox.Data.Models
{
    public class Company : Dictionary
    {
        [Required]
        public int EstablishmentYear {get;set;}

        public ICollection<Employee> Employees { get; set; }
	}
}