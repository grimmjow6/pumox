﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Pumox.Data.Models
{
	public interface IEntity
	{
		long Id { get; set; }
	}
}
