﻿using Newtonsoft.Json;


namespace Pumox.Data.Models
{
    public abstract class Entity : IEntity
    {

        [JsonIgnore]
        public long Id { get; set; }
    }
}