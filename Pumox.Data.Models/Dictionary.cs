﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Pumox.Data.Models
{
	public abstract class Dictionary : Entity
	{
        [Required]
		[StringLength(500)]
		public string Name { get; set; }
	}
}
