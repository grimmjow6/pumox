using System.ComponentModel;

namespace Pumox.Data.Models
{
    public enum EJobTitle : byte
    {
        [Description("Administrator")]
        Administrator = 1,

        [Description("Developer")]
        Developer = 2,

        [Description("Architekt")]
        Architect = 3,

        [Description("Menadzer")]
        Manager = 4

    }
}