﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using Pumox.Data;

namespace Pumox.Data.Migrations
{
    [DbContext(typeof(DataContext))]
    partial class DataContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn)
                .HasAnnotation("ProductVersion", "3.1.1")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("Pumox.Data.Models.Company", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<int>("EstablishmentYear")
                        .HasColumnType("integer");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("character varying(500)")
                        .HasMaxLength(500);

                    b.HasKey("Id");

                    b.ToTable("Companies");

                    b.HasData(
                        new
                        {
                            Id = -1L,
                            EstablishmentYear = 1990,
                            Name = "Furniture"
                        },
                        new
                        {
                            Id = -2L,
                            EstablishmentYear = 1995,
                            Name = "Cars"
                        },
                        new
                        {
                            Id = -3L,
                            EstablishmentYear = 2000,
                            Name = "Toys"
                        });
                });

            modelBuilder.Entity("Pumox.Data.Models.Employee", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<long?>("CompanyId")
                        .HasColumnType("bigint");

                    b.Property<DateTime>("DateOfBirth")
                        .HasColumnType("timestamp without time zone");

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasColumnType("character varying(500)")
                        .HasMaxLength(500);

                    b.Property<byte>("JobTitle")
                        .HasColumnType("smallint");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasColumnType("character varying(500)")
                        .HasMaxLength(500);

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.ToTable("Employees");

                    b.HasData(
                        new
                        {
                            Id = -1L,
                            CompanyId = -1L,
                            DateOfBirth = new DateTime(2001, 6, 13, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            FirstName = "Bartek",
                            JobTitle = (byte)1,
                            LastName = "Drugi"
                        },
                        new
                        {
                            Id = -2L,
                            CompanyId = -1L,
                            DateOfBirth = new DateTime(1991, 6, 13, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            FirstName = "Marcin",
                            JobTitle = (byte)2,
                            LastName = "Trzeci"
                        },
                        new
                        {
                            Id = -3L,
                            CompanyId = -2L,
                            DateOfBirth = new DateTime(1996, 6, 13, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            FirstName = "Daniel",
                            JobTitle = (byte)3,
                            LastName = "Czwarty"
                        });
                });

            modelBuilder.Entity("Pumox.Data.Models.Employee", b =>
                {
                    b.HasOne("Pumox.Data.Models.Company", "Company")
                        .WithMany("Employees")
                        .HasForeignKey("CompanyId");
                });
#pragma warning restore 612, 618
        }
    }
}
