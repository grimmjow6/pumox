﻿using Pumox.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pumox.Data.Configurations
{
    internal class CompanyConfiguration : IEntityTypeConfiguration<Company>
    {
        public void Configure(EntityTypeBuilder<Company> builder)
        {
            Sedd(builder);
        }

        private void Sedd(EntityTypeBuilder<Company> builder)
        {
            builder.HasData(
                new Company
                {
                    Id = -1L,
                    Name = "Furniture",
                    EstablishmentYear = 1990,
                }
                , new Company
                {
                    Id = -2L,
                    Name = "Cars",
                    EstablishmentYear = 1995,
                }
                , new Company
                {
                    Id = -3L,
                    Name = "Toys",
                    EstablishmentYear = 2000,
                }
            );
        }
    }
}
