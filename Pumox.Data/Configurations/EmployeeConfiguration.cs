﻿using Pumox.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pumox.Data.Configurations
{
	internal class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
	{
		public void Configure(EntityTypeBuilder<Employee> builder)
		{
			Sedd(builder);
		}

		private void Sedd(EntityTypeBuilder<Employee> builder)
		{
			builder.HasData(
				new Employee
                {
                    Id = -1L,
                    FirstName = "Bartek",
                    LastName = "Drugi",
                    DateOfBirth = new DateTime(2001, 06, 13),
                    JobTitle = EJobTitle.Administrator,
                    CompanyId = -1L
                }
				,new Employee
                {
                    Id = -2L,
                    FirstName = "Marcin",
                    LastName = "Trzeci",
                    DateOfBirth = new DateTime(1991, 06, 13),
                    JobTitle = EJobTitle.Developer,
                    CompanyId = -1L
                }
				, new Employee
                {
                    Id = -3L,
                    FirstName = "Daniel",
                    LastName = "Czwarty",
                    DateOfBirth = new DateTime(1996, 06, 13),
                    JobTitle = EJobTitle.Architect,
                    CompanyId = -2L
                }
			);
		}
	}
}
